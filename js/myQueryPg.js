myQueryPg = {
		
	run : function(querypg) {
		querypg.run();
	},
	
	getPageLogin : function(){
		// TODO: this should be done by the rest server
		html = "<div class=\"container\"><div class=\"row justify-content-md-center\"><div class=\"col-md-auto\">";
		html += "<div class=\"card\" style=\"max-width: 400px\">";
		html += "<div class=\"card-title\" style=\"text-align: center; background-color: rgb(0,123,255); color: white\">please log in</div>";
		html += "<div class=\"card-body\">";
		html += "<div class=\"container\"><div class=\"row\">";
		html += "<div class=\"col-sm\">username</div>";
		html += "<div class=\"col-sm\"><input type=\"text\" class=\"form-control\" placeholder=\"username\" onchange=\"querypg.setParam('username', this.value)\" /></div>";
		html += "</div><div class=\"row\">";
		html += "<div class=\"col-sm\">password</div>";
		html += "<div class=\"col-sm\"><input type=\"password\" class=\"form-control\" value=\"\" placeholder=\"password\" onchange=\"querypg.setParam('password', this.value)\" /></div>";
		html += "</div></div>";
		html += "<button type=\"button\" class=\"btn btn-primary\" onclick=\"myQueryPg.doCallRest(querypg, 'login')\">submit</button>";
		html += "</div></div></div></div></div>";
		querypg.setParam("username", undefined);
		querypg.setParam("password", undefined);
		return html;
	},
	
	doCallRest : function(querypg, page) {
		querypg.callRest(page);
		this.run(querypg);
	}
};
