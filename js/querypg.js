const HEAD = "head", BODY = "body", FOOT = "foot", MESSAGES = "messages";

querypg = {
	params : new Map(),

	callRest : function callRest(page) {
		// TODO: this should be done by the rest server
		if (page == "login") {
			this.doPageLogin();
		} else if (page == "logout") {
			this.doPageLogout();
		} else {
			this.setMessages("<div class=\"alert alert-danger show\" role=\"alert\">Error: page " + page + " not found</div>");
		}
	},
	
	// TODO: this should be done by the rest server
	doPageLogout : function doPageLogout() {
		this.setHead("");
		this.setMessages("");
	},
	
	// TODO: this should be done by the rest server
	doPageLogin : function doPageLogin() {
		// do some validation stuff
		username = this.getParam("username");
		password = this.getParam("password");
		this.setParam("username", undefined); // security
		this.setParam("password", undefined);
		if (username == "bob") {
			// some content from the rest api
			this.setBody("<table class=\"table\"><thead><tr><th scope=\"col\">col1</th><th scope=\"col\">col2</th></tr></thead><tbody><tr><td>one</td><td>two</td></tr></tbody></table>");
			this.setHead("<button type=\"button\" class=\"btn btn-primary\" onclick=\"querypg.setBody(myQueryPg.getPageLogin); myQueryPg.doCallRest(querypg, 'logout')\">logout</button>");
			this.setMessages("<div class=\"alert alert-success show\" role=\"alert\">welcome, " + username + "</div>");
		} else {
			this.setBody(myQueryPg.getPageLogin);
			this.setMessages("<div class=\"alert alert-danger show\" role=\"alert\">only bob is allowed to login, sorry...</div>");
		}
	},

	setParam : function setParam(key, value) {
		this.params.set(key, value);
	},
	
	setHead : function setHead(value) {
		this.params.set(HEAD, value);
	},
	
	setBody : function setBody(value) {
		this.params.set(BODY, value);
	},
	
	setFoot : function setFoot(value) {
		this.params.set(FOOT, value);
	},
	
	setMessages : function setMessages(value) {
		this.params.set(MESSAGES, value);
	},
	
	getParam : function getParam(key) {
		return(this.params.get(key));
	},
	
	getParams : function getParams() {
		return this.params;
	},
	
  run : function run() {
  	body = this.getParam(BODY);
  	head = this.getParam(HEAD);
  	foot = this.getParam(FOOT);
  	messages = this.getParam(MESSAGES);
  	if (head == undefined) {
  		head = "";
  	}
  	if (body == undefined) {
  		body = "";
  	}
  	if (foot == undefined) {
  		foot = "";
  	}
  	if (messages == undefined) {
  		messages = "";
  	}
  	$("#" + HEAD).html(head);
  	$("#" + BODY).html(body);
  	$("#" + FOOT).html(foot);
  	$("#" + MESSAGES).html(messages);
  }
};
